package com.example.abeete


import android.annotation.SuppressLint
import android.net.Uri
import android.os.Build
import android.os.Bundle
import android.provider.ContactsContract
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import android.widget.SimpleCursorAdapter
import androidx.fragment.app.Fragment
import androidx.recyclerview.widget.LinearLayoutManager
import androidx.recyclerview.widget.RecyclerView
import kotlinx.android.synthetic.main.fragment_contact_recycler_view.view.*


// TODO: Rename parameter arguments, choose names that match
// the fragment initialization parameters, e.g. ARG_ITEM_NUMBER
private const val ARG_PARAM1 = "param1"
private const val ARG_PARAM2 = "param2"

/**
 * A simple [Fragment] subclass.
 *
 */
@SuppressLint("InlinedApi")
private val FROM_COLUMNS:ArrayList<String> = arrayListOf(
    if((Build.VERSION.SDK_INT >= Build.VERSION_CODES.HONEYCOMB))
    {
        ContactsContract.Contacts.DISPLAY_NAME_PRIMARY
    }
else{
        ContactsContract.Contacts.DISPLAY_NAME
    }
)

private val TO_IDS:IntArray = intArrayOf(R.id.name)
class ContactRecyclerView : Fragment(){

    private lateinit var recycler_view:RecyclerView
    private lateinit var contactModelArrayList:ArrayList<ContactModel>
    var contactId:Long = 0
    var contactKey:String? = null
    var contactURI: Uri? = null
    var cursorAdapter:SimpleCursorAdapter? = null


    override fun onCreateView(
        inflater: LayoutInflater, container: ViewGroup?,
        savedInstanceState: Bundle?
    ): View? {
        // Inflate the layout for this fragment
        val view = inflater.inflate(R.layout.fragment_contact_recycler_view, container, false)
        recycler_view = view.recyclerview1
        contactModelArrayList = ArrayList()

        return view
    }


    override fun onActivityCreated(savedInstanceState: Bundle?) {
        super.onActivityCreated(savedInstanceState)


        recycler_view.layoutManager = LinearLayoutManager(activity!!.applicationContext)
        recycler_view.adapter = ContactAdapter(activity!!.application)
        recycler_view.setHasFixedSize(true)
    }
}
