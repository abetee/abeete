package com.example.abeete

import android.os.Bundle
import androidx.core.view.GravityCompat
import androidx.appcompat.app.AppCompatActivity

import kotlinx.android.synthetic.main.activity_main.drawer_layout

import kotlinx.android.synthetic.main.fragment_home.*

class MainActivity : AppCompatActivity(),Registration.RegisterButtonSelected, Home.OnFirstAidSelected
{


    override fun notNowButtonSelected() {
        val Home = ContactRecyclerView()
        supportFragmentManager.beginTransaction()
            .replace(android.R.id.content, Home)
            .commit()
    }

    override fun onFirstAidSelected() {

        val firstaid = FirstAidInfo()

        supportFragmentManager.beginTransaction()
            .replace(android.R.id.content, firstaid)
            .commit()


    }


    override fun registerButtonSelected() {
        setSupportActionBar(toolbar1);

        val Home = Home()
        supportFragmentManager.beginTransaction()
            .replace(android.R.id.content, Home)
            .commit()




    }

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setContentView(R.layout.activity_main)

        val register = Registration()

        supportFragmentManager.beginTransaction()
            .replace(android.R.id.content, register)
            .commit()
        }


    override fun onBackPressed() {
        if (drawer_layout.isDrawerOpen(GravityCompat.START)) {
            drawer_layout.closeDrawer(GravityCompat.START)
        } else {
            super.onBackPressed()
        }
    }


}
