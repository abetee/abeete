package com.example.abeete


import android.content.Context
import android.os.Bundle
import android.view.*
import androidx.fragment.app.Fragment
import androidx.appcompat.app.ActionBarDrawerToggle
import androidx.appcompat.app.AppCompatActivity
import androidx.appcompat.widget.Toolbar
import androidx.core.view.GravityCompat
import androidx.drawerlayout.widget.DrawerLayout
import com.google.android.material.navigation.NavigationView
import kotlinx.android.synthetic.main.fragment_home.*
import kotlinx.android.synthetic.main.fragment_home.drawer_layout
import kotlinx.android.synthetic.main.fragment_home.view.*


// TODO: Rename parameter arguments, choose names that match
// the fragment initialization parameters, e.g. ARG_ITEM_NUMBER
private const val ARG_PARAM1 = "param1"
private const val ARG_PARAM2 = "param2"

/**
 * A simple [Fragment] subclass.
 *
 */
class Home : Fragment(),NavigationView.OnNavigationItemSelectedListener {


    private lateinit var listerner: OnFirstAidSelected


    override fun onAttach(context: Context) {
        super.onAttach(context)
        if(context is OnFirstAidSelected)
        {
            listerner = context
        }



    }

    override fun onCreateView(
        inflater: LayoutInflater, container: ViewGroup?,
        savedInstanceState: Bundle?
    ): View? {
        // Inflate the layout for this fragment
        val view = inflater.inflate(R.layout.fragment_home, container, false)


        val firstAdid_CardView =view.firstaid


        firstAdid_CardView.setOnClickListener{
            listerner.onFirstAidSelected()
        }


        return view
    }


    override fun onOptionsItemSelected(item: MenuItem): Boolean {
        return when (item.itemId) {
            R.id.action_settings -> true
            else -> super.onOptionsItemSelected(item)
        }
    }
    override fun onNavigationItemSelected(item: MenuItem): Boolean {
        // Handle navigation view item clicks here.
        val fragment:Fragment
        when (item.itemId) {
            R.id.nav_home -> {
                fragment = Registration()
                activity!!.supportFragmentManager.beginTransaction()
                    .replace(android.R.id.content, fragment)
                    .commit()
            }
            R.id.nav_gallery -> {
                fragment = FirstAidInfo()
                activity!!.supportFragmentManager.beginTransaction()
                    .replace(android.R.id.content, fragment)
                    .commit()
            }
            R.id.nav_slideshow -> {
                fragment = VideoAid()
                activity!!.supportFragmentManager.beginTransaction()
                    .replace(android.R.id.content, fragment)
                    .commit()
            }
        }
        val drawerLayout: DrawerLayout =activity!!.findViewById(R.id.drawer_layout)
        drawerLayout.closeDrawer(GravityCompat.START)
        return true
    }


    override fun onActivityCreated(savedInstanceState: Bundle?) {
        super.onActivityCreated(savedInstanceState)
        val toolbar: Toolbar = toolbar1

        (activity as AppCompatActivity).setSupportActionBar(toolbar)
        val drawerLayout: DrawerLayout = drawer_layout

        val toggle = ActionBarDrawerToggle(
            activity, drawerLayout, toolbar, R.string.navigation_drawer_open, R.string.navigation_drawer_close
        )
        drawerLayout.addDrawerListener(toggle)
        toggle.syncState()
        val navView: NavigationView = activity!!.findViewById(R.id.nav_view)
        navView.setNavigationItemSelectedListener(this)
    }
    interface OnFirstAidSelected
    {
        fun onFirstAidSelected();
    }




}
