package com.example.abeete


import android.content.Context
import android.os.Bundle
import androidx.fragment.app.Fragment
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import android.widget.ArrayAdapter
import android.widget.Spinner
import kotlinx.android.synthetic.main.fragment_registration.*
import kotlinx.android.synthetic.main.fragment_registration.view.*


// TODO: Rename parameter arguments, choose names that match
// the fragment initialization parameters, e.g. ARG_ITEM_NUMBER
private const val ARG_PARAM1 = "param1"
private const val ARG_PARAM2 = "param2"

/**
 * A simple [Fragment] subclass.
 *
 */
class Registration : Fragment() {

    private lateinit var listerner:RegisterButtonSelected

    override fun onAttach(context: Context) {
        super.onAttach(context)
        if(context is RegisterButtonSelected)
        {
         listerner = context
        }

    }
    private lateinit var Spinner_tag:Spinner
    override fun onCreateView(
        inflater: LayoutInflater, container: ViewGroup?,
        savedInstanceState: Bundle?
    ): View? {
        // Inflate the layout for this fragment
        val view = inflater.inflate(R.layout.fragment_registration, container, false)
        val registerSelected= view.registerButton
        val notNowSelected = view.notnow

        registerSelected.setOnClickListener{listerner.registerButtonSelected()}
        notNowSelected.setOnClickListener { listerner.notNowButtonSelected() }


        return view
    }

    override fun onViewCreated(view: View, savedInstanceState: Bundle?) {
        super.onViewCreated(view, savedInstanceState)
        val bloodtype = arrayOf("O", "A", "AB")
        val adapter = ArrayAdapter(activity, android.R.layout.simple_spinner_item, bloodtype)
        adapter.setDropDownViewResource(android.R.layout.simple_dropdown_item_1line)
        spinner.adapter = adapter
    }

   interface RegisterButtonSelected
   {
        fun registerButtonSelected();
       fun notNowButtonSelected();
   }

}
