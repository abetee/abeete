package com.example.abeete

import android.content.Context
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import androidx.recyclerview.widget.RecyclerView
import kotlinx.android.synthetic.main.fragment_all_contacts.view.*

class ContactAdapter(val context: Context):RecyclerView.Adapter<ContactViewHolder>()
{

    private val contacts = listOf(

        Contacts("sjygsjfds", "sdflkusfdkus"),
        Contacts("sjygsjfds", "sdflkusfdkus"),
        Contacts("sjygsjfds", "sdflkusfdkus"),
        Contacts("sjygsjfds", "sdflkusfdkus")
    )
    override fun onCreateViewHolder(parent: ViewGroup, viewType: Int): ContactViewHolder {
        val inflater = LayoutInflater.from(parent.context)
        val recyclerView = inflater.inflate(R.layout.fragment_all_contacts, parent, false)
        return ContactViewHolder(recyclerView)
    }

    override fun getItemCount(): Int {
       return contacts.size
    }

    override fun onBindViewHolder(holder: ContactViewHolder, position: Int) {
            val contact = contacts[position]
        holder.itemView.name.text = contact.name
        holder.itemView.phone.text = contact.phoneNumber
    }


}
class ContactViewHolder(itemView: View):RecyclerView.ViewHolder(itemView)
{}